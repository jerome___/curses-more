"""
Test views packages for
 - abstract module
 - concrete forms module
 """

import pytest
from zope.interface.verify import verifyClass, verifyObject

from views.forms import FormView
from views.abstract import FormBaseView


def test_no_abstract_instance():
    """Can not create an instance of abstract class"""

    with pytest.raises(TypeError, match=r".*function missing.*"):
        FormBaseView()


def test_form_implement_form_view():
    """FormView implement FormBaseView interface"""

    assert verifyClass(FormBaseView, FormView)


def test_form_instanciate_form_view():
    """Instance of FormView get FormBaseView abstract methods parent"""

    assert verifyObject(FormBaseView, FormView(test=True))
