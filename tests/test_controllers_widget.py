"""
Test controllers packages for
 - abstract module
 - concrete widget module
 """

import pytest
from zope.interface import implementer
from zope.interface.verify import verifyClass, verifyObject

from controllers.elements import Widget, Element, ElementNameExistAlready
from controllers.abstract import WidgetBase
from controllers.errors import TypeErrorAttributes


@implementer(WidgetBase)
class WidgetError(Element):
    """Test should failed"""


    def __init__(self, **kwargs):

        super().__init__(**kwargs)
        self._model = 10
        self._view = "error"

    def update(self):
        """Update this content's objects statement"""
        pass

    @property
    def model(self):
        return self._model

    @property
    def view(self):
        return self._view

    @property
    def name(self):
        return self._name

    def __repr__(self):
        return "\nmodel = {} | view = {}" \
            .format(type(self._model), type(self._view))


def test_attributes_property_present():
    """FormMenu have property attributes method present"""

    widget = Widget(name="test1", test=True)
    assert hasattr(widget, 'model'), "You have to have a Form model: FormMenuModel"
    assert hasattr(widget, 'view'), "You have to have a Form view: FormMenuView"
    del widget


def test_no_abstract_instance():
    """Can not create an instance of abstract class"""

    with pytest.raises(TypeError, match=r".*function missing.*"):
        WidgetBase()


def test_wrong_attributes_type():
    """WidgetError should not have wrong attributes type instance for self.model and self.view"""

    with pytest.raises(TypeErrorAttributes, match=r'.*(model|view).*'):
        WidgetBase.validateInvariants(WidgetError(name='test1'))


def test_widget_name_unique():
    """Widget has unique name or raise Error"""

    with pytest.raises(ElementNameExistAlready):
        widget_1 = Widget(name='test', test=True)
        widget_2 = Widget(name='test', test=True)


def test_widget_implement_form_controller():
    """Form implement FormController interface"""

    assert verifyClass(WidgetBase, Widget)


def test_widget_instanciate_form_controller():
    """Instance of Form get FormBaseController abstract methods"""

    assert verifyObject(WidgetBase, Widget(name="test", test=True))
