"""
Test controllers packages for
 - abstract module
 - concrete Layout module
 """

import pytest
from zope.interface import implementer
from zope.interface.verify import verifyClass, verifyObject

from controllers.elements import Layout, Element
from controllers.abstract import ContainerBase
from controllers.errors import TypeErrorAttributes


@implementer(ContainerBase)
class LayoutError(Element):
    """Test should failed"""

    def __init__(self, **kwargs):

        super().__init__(**kwargs)
        self._model = 10
        self._view = "error"

    def update(self):
        """Update this content's objects statement"""
        pass

    @property
    def model(self):
        return self._model

    @property
    def view(self):
        return self._view

    def __repr__(self):
        return "\nmodel = {} | view = {}" \
            .format(type(self._model), type(self._view))


def test_attributes_property_present():
    """FormMenu have property attributes method present"""

    layout = Layout(name='test_layout', test=True)
    assert hasattr(layout, 'model'), "You have to have a Form model: FormMenuModel"
    assert hasattr(layout, 'view'), "You have to have a Form view: FormMenuView"
    del layout


def test_no_abstract_instance():
    """Can not create an instance of abstract class"""

    with pytest.raises(TypeError, match=r".*function missing.*"):
        ContainerBase()


def test_wrong_attributes_type():
    """WidgetError should not have wrong attributes type instance for self.model and self.view"""

    with pytest.raises(TypeErrorAttributes, match=r'.*(model|view).*'):
        ContainerBase.validateInvariants(LayoutError())


def test_container_implement_form_controller():
    """Form implement FormController interface"""

    assert verifyClass(ContainerBase, Layout)


def test_container_instanciate_form_controller():
    """Instance of Form get FormBaseController abstract methods"""

    assert verifyObject(ContainerBase, Layout(name='test_layout', test=True))
