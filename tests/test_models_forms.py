"""
Test models packages for
 - abstract module
 - concrete forms module
 - theme_parser module
 """

import pytest
from zope.interface.verify import verifyClass, verifyObject

from models.forms import FormModel
from models.abstract import FormBaseModel
from models.theme_parser import *
from models.errors import *


def test_no_abstract_instance():
    """Can not create an instance of abstract class"""

    with pytest.raises(TypeError, match=r".*function missing.*"):
        FormBaseModel()


def test_form_implement_form_model():
    """FormModel implement FormBaseModel interface"""

    assert verifyClass(FormBaseModel, FormModel)


def test_form_instanciate_form_model():
    """Instance of FormModel get FormBaseModel abstract methods"""

    assert verifyObject(FormBaseModel, FormModel(1, test=True))


def test_theme_parser_build_theme():
    """Test if models.theme_parser BuildTheme can do the job to detect XML error setting entries"""

    # Test missing xml file setup for theme/color definition
    with pytest.raises(FileNotFoundError, match=r"Theme file .*\.xml doesn't exist or not there"):
        BuildTheme(theme_file=Path('_.xml'), test=True)
    # Test missing  'title' attribute for color tag from setup theme/config file
    with pytest.raises(MissingAttribute, match='Missing a title attribute for "color" theme tag'):
        BuildTheme(theme_file=Path('./tests/themes/missing_color_title.xml'), test=True)
    # Test missing 'convention' attribute for color tag from setup theme/config file
    with pytest.raises(MissingAttribute,
                       match='Missing attribute convention for color tag to convert RGB color values'):
        BuildTheme(theme_file=Path('./tests/themes/missing_color_convention.xml'), test=True)
    # Test Wrong convention attribute value (not in Convention Enum)
    with pytest.raises(UnknownConvention, match=r'Unknown convention value to convert color RGB values: wrong'):
        BuildTheme(theme_file=Path('./tests/themes/wrong_color_convention.xml'), test=True)
    # Missing 'red' sub color tag from setup theme/colors xml file
    with pytest.raises(MissingTag, match='Missing tag "red" for "color" theme tag'):
        BuildTheme(theme_file=Path('./tests/themes/missing_red_color_tag.xml'), test=True)
    # Missing 'green' sub color tag from setup theme/colors xml file
    with pytest.raises(MissingTag, match='Missing tag "green" for "color" theme tag'):
        BuildTheme(theme_file=Path('./tests/themes/missing_green_color_tag.xml'), test=True)
    # Missing 'blue' sub color tag from setup theme/colors xml file
    with pytest.raises(MissingTag, match='Missing tag "blue" for "color" theme tag'):
        BuildTheme(theme_file=Path('./tests/themes/missing_blue_color_tag.xml'), test=True)
    # Test missing  'title' attribute for theme tag from setup theme/config file
    with pytest.raises(MissingAttribute, match='Missing a title attribute for "tag" theme tag'):
        BuildTheme(theme_file=Path('./tests/themes/missing_tag_title.xml'), test=True)
    # Test Wrong title attribute value (not in ThemeTags Enum)
    with pytest.raises(UnknownTag, match=r"Unknown <theme><tag>.title attribute value: 'wrong' to use theme with"):
        BuildTheme(theme_file=Path('./tests/themes/wrong_tag_title.xml'), test=True)
    # Missing foreground tag's tag child
    with pytest.raises(MissingTag, match='Missing tag "foreground" for "tag" theme tag'):
        BuildTheme(theme_file=Path('./tests/themes/missing_foreground_tag.xml'), test=True)
    # Unknown foreground text from Colors
    with pytest.raises(UnknownColor, match="Unknown Color.COLOR key for <theme><tag><foreground> value: unknown"):
        BuildTheme(theme_file=Path('./tests/themes/wrong_foreground_text.xml'), test=True)
    # Missing background tag's tag child
    with pytest.raises(MissingTag, match='Missing tag "background" for "tag" theme tag'):
        BuildTheme(theme_file=Path('./tests/themes/missing_background_tag.xml'), test=True)
    # Unknown background text from Colors
    with pytest.raises(UnknownColor, match="Unknown Color.COLOR key for <theme><tag><background> value: unknown"):
        BuildTheme(theme_file=Path('./tests/themes/wrong_background_text.xml'), test=True)
