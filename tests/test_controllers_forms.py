"""
Test controllers packages for
 - abstract module
 - concrete forms module
 """

import pytest
from zope.interface import implementer
from zope.interface.verify import verifyClass, verifyObject

from controllers.forms import Form
from controllers.abstract import FormBase
from controllers.errors import TypeErrorAttributes


@implementer(FormBase)
class FormError:
    """Test should failed"""

    def __init__(self, **kwargs):

        self._model = 10
        self._view = "error"

    def update(self):
        """Update this content's objects statement"""
        pass

    @property
    def model(self):
        return self._model

    @property
    def view(self):
        return self._view

    def __repr__(self):
        return "\nmodel = {} | view = {}"\
            .format(type(self._model), type(self._view))


def test_attributes_property_present():
    """FormMenu have property attributes method present"""

    form = Form(name='test1', test=True)
    assert hasattr(form, 'model'), "You have to have a Form model: FormMenuModel"
    assert hasattr(form, 'view'), "You have to have a Form view: FormMenuView"


def test_no_abstract_instance():
    """Can not create an instance of abstract class"""

    with pytest.raises(TypeError, match=r".*function missing.*"):
        FormBase()


def test_wrong_attributes_type():
    """FormError should not have wrong attributes type instance for self.model and self.view"""

    with pytest.raises(TypeErrorAttributes, match=r'.*(model|view).*'):
        FormBase.validateInvariants(FormError())


def test_form_implement_form_controller():
    """Form implement FormController interface"""

    assert verifyClass(FormBase, Form)


def test_form_instanciate_form_controller():
    """Instance of Form get FormBaseController abstract methods"""

    assert verifyObject(FormBase, Form(name='test2', test=True))
