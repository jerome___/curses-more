![Logo](images/logo_horizontal_curses-more.png)
# ncurses-more #

This is a Python-3.6+ curses MVC framework project with GPL-v3.0 Licence.

You would be able to use it for easily create terminal applications ui with 
Forms, Menus, Widgets, Grid positioning, command caller objects, Popup containers and more. 

### Framework Objects ###

* Form (is a main window container to contain any other more-curses objects inside and have linkers to other Forms)
* Grid (is a positioning widgets content)
* Command container (is a command object to call easy from any widget actions)
* Widget (is an abject ui container to print something and get action from keyboard or mouse)
* Event listener (is an helper to react from any event and do action on own or other Form objects)
* Popup container (is a mini Form to contain any widgets inside)


### Python-3 std libraries imported ###

* curses
* datetime
* libpath
* Enum
* logging
* xml

### Python 3rd party packages

* zope.interface
* zope.event
* pydantic
* pytest

### Demonstrations files list ###

* Simple Form with minimal widgets used

### Tests ###

#### Controllers

* abstract FormBaseController
* Form

#### Models

* abstract FormBaseModel
* FormModel

#### Views

* abstract FormBaseView
* FormView

## How to use ##

```python
from curses-more import *
```
