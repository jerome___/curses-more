"""Forms Models module"""

from zope.interface import implementer

from .abstract import FormBaseModel
from .theme_parser import BuildTheme
from .tools import LayoutPosition
from controllers.elements import Layout


@implementer(FormBaseModel)
class FormModel:
    """Model Form used by Form controller"""

    def __init__(self, number: int, **kwargs):

        self._title: str = " {} ".format(kwargs['title']) if kwargs.get('title') else " Form {} ".format(number)
        self._border: bool = kwargs.get('border') or False
        self._elements = list()  # contain list of widgets and containers (can be 2D list)
        self._layout = kwargs.get('layout') if isinstance(kwargs.get("layout"), Layout) else None
        self._colors = None
        self._theme = None
        self._selected = None

    def __del__(self):

        pass

    @property
    def title(self) -> str:
        """Get title"""

        return self._title

    @title.setter
    def title(self, title: str):
        """Set title"""

        self._title = title

    @property
    def border(self) -> bool:
        """Get border"""

        return self._border

    @border.setter
    def border(self, enabled: bool):
        """Set border enabled"""

        self._border = enabled

    @property
    def theme(self):
        """Get theme directory path"""

        return self._theme

    @theme.setter
    def theme(self, kwargs):
        """Setup theme (and colors) from 'theme_file' kwargs pathlib.Path value"""

        theme_builder = BuildTheme(**kwargs)
        self._colors = theme_builder.colors if theme_builder.exist else False
        self._theme = theme_builder.theme if theme_builder.exist else False

    @property
    def colors(self):
        """Get colors"""

        return self._colors

    @property
    def selected(self) -> LayoutPosition:
        """Get selected content location"""

        return self._selected

    @selected.setter
    def selected(self, layout_position: LayoutPosition):
        """Setup cursor position"""

        self._selected = layout_position

    @property
    def elements(self):
        """Get widgets (any widgets, menus)"""

        return self._elements

    @elements.setter
    def elements(self, elements):
        """Set widgets"""

        self._elements = elements

    @property
    def layout(self):
        """Get elements (any widgets, menus)"""

        return self._layout

    @layout.setter
    def layout(self, layout):
        """Set elements"""

        self._layout = layout

    @layout.deleter
    def layout(self):
        """Delete elements"""

        del self._layout
