"""Abstract Models classes to be use by controllers objects of curse-more framework to buil any"""

from zope.interface import Interface, Attribute

from .tools import Status
from .theme_parser import Colors, Theme


class FormBaseModel(Interface):
    """Form Model abstract class to be inherited to build a real FormModel object from"""

    title: str = Attribute("Title of Form")
    border: bool = Attribute("Has a border ?")
    theme: Theme = Attribute("color Theme")
    colors: Colors = Attribute("Colors")
    selected: str = Attribute("Widget name selected")
    elements: list = Attribute("Form content widgets list()")
    layout = Attribute("Has a Layout container to position widgets content")


class ContainerBaseModel(Interface):
    """Model container"""

    status: Status = Attribute("The state of this element though an ENUM Status typed list of choices")
    name: str = Attribute("Layout unique name")


class WidgetBaseModel(Interface):
    """
    Model of widget to amb content data
    and /or link with other source
    """

    label: str = Attribute("Widget has a label")
    content = Attribute("Widget has a content to embed data (any type able)")
    position = Attribute("Widget has a position in relation with Layout (it can be list of int or int")
