"""Customized Models Errors module"""


class CustomError(Exception):
    """Customized __repr__ for my customs errors"""

    def __repr__(self):
        """Return docstring"""

        return self.__doc__


class MissingTag(CustomError):
    """Missing tag error"""

    pass


class MissingAttribute(CustomError):
    """Missing an attribute error"""

    pass


class UnknownTag(CustomError):
    """Unknown tag error"""

    pass


class UnknownColor(CustomError):
    """Color doesn't exist... hasn't been created"""

    pass


class UnknownConvention(CustomError):
    """Convention doesn't exist"""

    pass


class MissingThemeTitle(CustomError):
    """Title definition is missing in Theme config file"""

    pass
