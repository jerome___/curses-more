"""Model tools"""

from enum import Enum
from collections import namedtuple


Coordinates = namedtuple('Coordinates', ['x', 'y'])
Size = namedtuple('Size', ['height', 'width'])
LayoutPosition = namedtuple('LayoutPosition', [ 'name', 'line', 'column'])


class Status(Enum):
    """Status for Layout and Widget"""

    Enabled = 0
    Disabled = 1
    Hidden = 2
    Blink = 3
    Edited = 4
