"""Elements module contain:
 _ LayoutModel concrete parent container
 _ WidgetModel concrete parent container"""

from abc import ABC, abstractmethod

from zope.interface import implementer
from collections import abc
from .abstract import ContainerBaseModel, WidgetBaseModel
from .tools import Status


class LayoutMissingStatusError(Exception):
    """Missing key argument 'status' as Status type, required to initialize Layout child object"""


@implementer(ContainerBaseModel)
class LayoutModel(ABC, abc.MutableSequence):
    """Model for Layout container parent.
    It is not a concrete class and can not be directly implemented
    Because collections.abc.MutableSequence parent, child inherit from
    this LayoutModel abstract class should implement:
     __delitem__, __getitem__, __setitem__, __len__, insert"""

    @abstractmethod
    def __init__(self, **kwargs):
        self._status: Status = kwargs.get('status')
        if self._status is None:
            raise LayoutMissingStatusError("Missing status for Layout")
        self._content = list()

    @property
    def status(self):
        """Get status of Layout"""

        return self._status

    @status.setter
    def status(self, status):
        """Setting status of Layout"""

        self._status = status

    @abstractmethod
    def __add__(self, element):
        """Append a content to the content_list (Container or Widget view)"""
        pass

    def __repr__(self):
        """Return my name and my content"""

        return "{} : {}".format(self.__class__.__name__, self._content)


class ListLayoutModel(LayoutModel):
    """List of content for Layout model (VerticalLayout and HorizontalLayout)"""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def __add__(self, element):
        """Append a content to the content_list (Container or Widget view)"""

        self._content.append(element)
        return self

    def insert(self, element, position):
        """Insert a content at position relative place in the content_list"""

        self._content.insert(position, element)

    def __delitem__(self, position):
        """Remove an object from positioned content list"""

        del self._content[position]

    def __getitem__(self, index: int):
        """Get Layout item on index as int position"""

        return self._content[index]

    def __setitem__(self, position, element):
        """Set content Layout Elements"""

        self._content[position] = element

    def __len__(self):

        return len(self._content)


class TableLayoutModel(LayoutModel):
    """2 dimensions content Layout model (GridLayout) as list of list"""

    def __init__(self, **kwargs):
        super().__init__(*kwargs)

    def __add__(self, element):
        """Append a content to the content_list (Container or Widget view)"""

        self._content[-1].append(element)
        return self

    def insert(self, element, position):
        """Insert a content at position relative place in the content_list"""

        self._content[position[0]].insert(position[1], element)

    def __delitem__(self, position):
        """Remove an object from positioned content list"""

        del self._content[position[0]][position[1]]
        if len(self._content[position[0]]) == 0:
            del self._content[position[0]]

    def __getitem__(self, index: tuple):
        """Get Layout item on index as tuple position: tuple(line: int, column: int)"""

        return self._content[index[0]][index[1]]

    def __setitem__(self, position, element):
        """Set content Layout Elements"""

        self._content[position[0]][position[1]] = element

    def __len__(self):
        return len(self._content)


@implementer(WidgetBaseModel)
class WidgetModel:
    """Base for all Widget Models to get minimal required attributes and methods"""

    def __init__(self, **kwargs):
        self._label = kwargs.get("label") or ""
        self._content = kwargs.get("content") or None

    @property
    def label(self):
        """Get label of Widget"""

        return self._label

    @label.setter
    def label(self, label):
        """Setup label text content"""

        self._label = label

    @property
    def content(self):
        """Get Widget content"""

        return self._content
