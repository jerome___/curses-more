"""
Theme parser:
- Color definition
- Link usual tags with foreground & background colors

Usual tags to be defined are in Enum class 'ThemeTag'
"""

import curses
from enum import Enum
from xml.etree import ElementTree as ETree
from pathlib import Path

from .errors import MissingTag, MissingAttribute, UnknownConvention, UnknownTag, UnknownColor, MissingThemeTitle
from config.setting import DEFAULT_THEME_FILE


class Convention(Enum):
    """Convention to use to define/convert color to use with curses colors RGB values"""

    decimal = 0
    hexadecimal = 1
    curses_value = 2


class ThemeTags(Enum):
    """Tags list usable for theme"""

    TITLE_FORM = 0
    TITLE_WIDGET = 1
    TITLE_POPUP = 2
    LABEL_WIDGET = 3
    CONTENT_WIDGET = 4
    FORM_HIGHLIGHT = 5
    WIDGET_HIGHLIGHT = 6
    CONTENT_HIGHLIGHT = 7
    WIDGET_EDIT = 8
    WARING = 8
    ERROR = 9
    ALERT = 11
    DISABLED = 12


def check_has_error(caller, **kwargs) -> bool:
    """Raise custom error is there is something wrong in the XML config file"""

    if isinstance(caller, Colors):
        if not kwargs.get('title'):
            raise MissingAttribute('Missing a title attribute for "color" theme tag')
        if not kwargs.get('convention'):
            raise MissingAttribute("Missing attribute convention for color tag to convert RGB color values")
        elif kwargs.get('convention') not in Convention.__members__.keys():
            error = "Unknown convention value to convert color RGB values: {}".format(kwargs.get('convention'))
            raise UnknownConvention(error)
        if not kwargs.get('red'):
            raise MissingTag('Missing tag "red" for "color" theme tag')
        if not kwargs.get('green'):
            raise MissingTag('Missing tag "green" for "color" theme tag')
        if not kwargs.get('blue'):
            raise MissingTag('Missing tag "blue" for "color" theme tag')
    elif isinstance(caller, Theme):
        colors = Colors()
        if not kwargs.get('title'):
            raise MissingAttribute('Missing a title attribute for "tag" theme tag')
        elif kwargs.get('title') not in ThemeTags.__members__.keys():
            error = "Unknown <theme><tag>.title attribute value: '{}' to use theme with".format(kwargs.get('title'))
            raise UnknownTag(error)
        if not kwargs.get('foreground'):
            raise MissingTag('Missing tag "foreground" for "tag" theme tag')
        elif not colors.has(kwargs.get('foreground')):
            error = "Unknown Color.COLOR key for <theme><tag><foreground> value: {}".format(kwargs.get('foreground'))
            raise UnknownColor(error)
        if not kwargs.get('background'):
            raise MissingTag('Missing tag "background" for "tag" theme tag')
        elif not colors.has(kwargs.get('background')):
            error = "Unknown Color.COLOR key for <theme><tag><background> value: {}".format(kwargs.get('background'))
            raise UnknownColor(error)
    return True


def build_theme_attributes(visited):
    """Build attributes of visited instance from his own class attribute"""

    class ClassAttributes:
        """Build object with attributes from dict content recursively
        with 'foreground' or 'background' and 'value' as key for other non dict own values"""

        def __init__(self, **kwargs):

            new_attributes = dict()
            for k, v in kwargs.items():
                if isinstance(v, dict) and k in ThemeTags.__members__.keys():
                    new_attributes.update({k: self.set_attribute_deeper(**v)})
                elif k in ['foreground', 'background', 'value', 'color']:
                    new_attributes.update({k: v})
            self.__dict__.update(new_attributes)

        @classmethod
        def set_attribute_deeper(cls, **deeper):
            """Get own class created with deeper attributes inside"""

            return cls(**deeper)

    new_attributes = ClassAttributes(**visited.all)
    visited.__dict__.update(new_attributes.__dict__)


def theme(theme_tag: ThemeTags):
    """Get color from his name if exist or 0"""

    try:
        return getattr(Theme(), theme_tag.name)
    except Exception:
        raise MissingThemeTitle("'{}' is not defined from theme config xml file".format(theme_tag.name))


class Theme:
    """Theme of tags to  use"""

    __THEME: dict = dict()

    def __init__(self, **kwargs):

        if not self.is_empty():
            build_theme_attributes(self)
        self._test = kwargs.get('test') or False

    def add(self, **kwargs):
        """Add THEME with kwargs keys as title, convention, red, green, blue"""

        check_has_error(self, **kwargs)
        title = kwargs.get('title').upper()
        foreground = kwargs.get('foreground').upper()
        background = kwargs.get('background').upper()
        value = ThemeTags[title].value + 1
        if self._test:
            Theme.__THEME[title] = dict(foreground=foreground, background=background, value=value)
        else:
            curses.init_pair(value, color(foreground), color(background))
            color_pair = curses.color_pair(value)
            Theme.__THEME[title] = dict(foreground=foreground, background=background, value=value, color=color_pair)

    @property
    def all(self):
        """Get own class attribute __THEME"""

        return Theme.__THEME

    def has(self, title) -> bool:
        """Tell if Theme  and ThemeTags contains title Theme"""

        return hasattr(ThemeTags, title) and title in Theme.__THEME

    @staticmethod
    def is_empty():
        """Tell if Colors is empty"""

        return len(Theme.__THEME) == 0


def convert_rgb(convention: Convention, red: int, green: int, blue: int) -> tuple:
    """Convert and return RGB values due to Convention"""

    factor = float(999 // 255)
    if convention == Convention.decimal:
        return tuple(map(lambda x: int(int(x) * factor), [red, green, blue]))
    elif convention == Convention.hexadecimal:
        return tuple(map(lambda x: int(int(x, 16) * factor), [red, green, blue]))
    return int(red), int(green), int(blue)


def color(name) -> int:
    """Get color from his name if exist or 0"""

    return Colors().get(name)


class Colors:
    """Colors used by Theme or to be call"""

    __COLORS = dict()

    def __init__(self, **kwargs):

        self._test = kwargs.get('test') or False

    def add(self, **kwargs):
        """Add to COLORS with kwargs keys as title, foreground, background"""

        check_has_error(self, **kwargs)
        convention = Convention[kwargs.get('convention')]
        title = kwargs.get('title').upper()
        red, green, blue = convert_rgb(convention, kwargs.get('red'), kwargs.get('green'), kwargs.get('blue'))
        color_number = len(Colors.__COLORS) + 16
        if not self._test:
            curses.init_color(color_number, red, green, blue)
        Colors.__COLORS[title] = color_number

    def has(self, name):
        """Tell if Colors has name color"""

        return name in Colors.__COLORS.keys()

    def get(self, name):
        """Get color from name"""

        return Colors.__COLORS.get(name)

    def names(self) -> tuple:
        """Get colors names available"""

        return Colors.__COLORS.keys() if Colors.__COLORS else tuple()

    @staticmethod
    def is_empty():
        """Tell if Colors is empty"""

        return len(Colors.__COLORS) == 0


class BuildTheme:
    """Color & Theme factory to apply for curses.more"""

    __ROOT = None

    def __init__(self, **kwargs):

        file: Path = kwargs.get('theme_file') or DEFAULT_THEME_FILE
        self.__exist = False
        self._them = None
        self._colors = None
        if file.exists() and file.is_file():
            if not kwargs.get('test'):
                curses.start_color()
            self._theme = Theme(test=kwargs.get('test'))
            self._colors = Colors(test=kwargs.get('test'))
            self.parse(file)
            self._exist = True
        else:
            raise FileNotFoundError("Theme file {} doesn't exist or not there".format(file.resolve()))

    def parse(self, file):
        """Build Theme and Colors definitions from XML file parsed"""

        BuildTheme.__ROOT = ETree.parse(file).getroot()
        for child in BuildTheme.__ROOT:
            attributes: dict = child.attrib
            elements = dict([element.tag, element.text] for element in child)
            if child.tag == 'color':
                self._colors.add(**attributes, **elements)
            elif child.tag == 'tag':
                self._theme.add(**attributes, **elements)
        build_theme_attributes(self._theme)

    @property
    def colors(self):
        """Get self Color instance"""

        return self._colors

    @property
    def theme(self):
        """Get self._theme instance"""

        return self._theme

    @property
    def exist(self):
        """Does Theme and Colors exist ?"""

        return self._exist
