"""Elements module provide:
 _ LayoutView parent concrete container
 _ WidgetView parent concrete container"""

from zope.interface import implementer
from abc import ABC, abstractmethod

from .abstract import ContainerBaseView, WidgetBaseView
from views.tools import Size, Coordinates
from .forms import Coordinates


@implementer(ContainerBaseView)
class LayoutView(ABC):
    """Abstract container controller to View  side"""

    @abstractmethod
    def __init__(self, **kwargs):

        self._position = Coordinates()

    @property
    def position(self):
        """Get position vector value"""

        return self._position

    @abstractmethod
    def draw_decorum(self):
        """Design content of container"""

        pass

    @abstractmethod
    def draw_data_label(self):
        """Design the data content"""

        pass

    @abstractmethod
    def draw_data_content(self):
        """Design the data content"""

        pass

    @abstractmethod
    def refresh(self):
        """Refresh the view"""

        pass


class VerticalLayoutView(LayoutView):
    """View for VerticalLayout Elements"""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def draw_data_label(self):
        pass

    def draw_data_content(self):
        pass

    def draw_decorum(self):
        pass

    def refresh(self):
        pass


class HorizontalLayoutView(LayoutView):
    """View for HorizontalLayout Elements"""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def draw_data_label(self):
        pass

    def draw_data_content(self):
        pass

    def draw_decorum(self):
        pass

    def refresh(self):
        pass


class GridLayoutView(LayoutView):
    """View for GridLayout Elements"""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def draw_data_label(self):
        pass

    def draw_data_content(self):
        pass

    def draw_decorum(self):
        pass

    def refresh(self):
        pass


class WidgetMissingSize(Exception):
    """Widget has no size value. It should have one"""


class WidgetWrongSizeType(Exception):
    """Wrong Widget view size type. Has to be Size typed"""


class WidgetWrongPositionType(Exception):
    """Wrong widget view position type. Have to be Coordinates typed."""


@implementer(WidgetBaseView)
class WidgetView:
    """Minimal Widget View parent"""

    def __init__(self, **kwargs):

        self._screen = None
        self._margin = kwargs.get("margin") or 0
        self._padding = kwargs.get('padding') or 0
        if not kwargs.get('test'):
            self.label_width = len(kwargs.get('label') or "") + 2
            self._screen = kwargs.get('screen')
            self._size = kwargs.get("size")
            self._position = kwargs.get("position") or 0
            self.can_build_widget()

    def can_build_widget(self):
        """Test if Widget can be build, raise a specific error if not"""

        if self._size is None:
            raise WidgetMissingSize("Please, provide a size=Size(height=<int>, width=<int>)")
        if not isinstance(self._size, Size):
            raise WidgetWrongSizeType("Please, provide a 'Size' type to size value to pass to widget view")
        if not isinstance(self._position, Coordinates):
            raise WidgetWrongPositionType("Please provide a 'Coordinates' type to pass top position for widget view")


    def draw(self):
        """Draw widget minimal content"""

        if self._screen:
            pass

    @property
    def screen(self):
        """Get curses.initscr() screen used"""
        return self._screen

    @screen.setter
    def screen(self, screen):
        """Setup Widget curses.initscr() screen"""

        self._screen = screen

    @property
    def margin(self):
        """Get margin of Widget"""

        return self._margin

    @property
    def padding(self):
        """get Widget padding"""

        return self._padding

    @property
    def position(self):
        """Get position Widget"""

        return self._position

