"""Forms views module models"""

from zope.interface import implementer
import curses
from collections import namedtuple

from .abstract import FormBaseView
from .tools import define_edges, Coordinates
from models.theme_parser import ThemeTags, theme, color


WindowLimit = namedtuple('WindowLimit', ["TopLeft", "BottomRight"])


@implementer(FormBaseView)
class FormView:
    """View Form used by Form controller"""

    def __init__(self, **kwargs):

        self._window_limit = WindowLimit(Coordinates(0, 0), Coordinates(1, 1))
        if kwargs.get("test"):
            self._window = None
        else:
            self._screen = curses.initscr()
            self._position = kwargs.get('position') or Coordinates(x=0, y=0)
            self._edges = define_edges()
            self._window = self._screen.derwin(self._edges.y, self._edges.x,
                                               self.position.y, self._position.x)  # Setup window area

    def __del__(self):

        if hasattr(self, '_screen'):
            curses.endwin()

    def draw(self, border: bool, title: str):
        """Draw the Form window"""

        # setup title with border if border True
        if border:
            self._screen.border(0)
            self._screen.addstr(0, 4, title)
        else:  # draw background colored first top line
            _title = "   " + title + " " * (self._edges.x - len(title) - 2)
            self._screen.addstr(0, 0, _title, theme(ThemeTags.Title_FORM).color)
        self.window_limit = border

    def refresh(self):
        """Refresh the view for FormMenu"""

        curses.noecho()
        self._screen.refresh()
        self._window.refresh()
        self.setup_layer()

    def setup_layout(self):
        """Define the basic layer window"""

        pass

    def get_key(self):
        """Get key event"""

        self._screen.keypad(True)
        return self._screen.getkey()

    @property
    def window(self):
        """Get curses.subwin sub window"""

        return self._window

    @property
    def window_limit(self) -> WindowLimit:
        """Get window limit coordinates (top_left and bottom_right"""

        return self._window_limit

    @window_limit.setter
    def window_limit(self, border: bool):
        """Set window limit
        values.border: bool => has border ?
        values.bottom_right: Coordinate => bottom left Coordinate"""

        self._window_limit = WindowLimit(TopLeft=Coordinates(x=self._position.x + 1,
                                                             y=self._position.y + 1),
                                         BottomRight=Coordinates(x=self._edges.x - 1,
                                                                 y=self._edges.y - 1)) if border \
            else WindowLimit(TopLeft=self._position, BottomRight=self._edges)

    @property
    def position(self):
        """Position property"""

        return self._position
