"""Abstract Views classes to be inherited to build curses-more view of ui objects from"""

from zope.interface import Interface, Attribute

from models.tools import Coordinates


class FormBaseView(Interface):
    """Form View abstract class to be inherited to build a Form ui view object from"""

    window = Attribute("curses.subwin object")
    window_limit: Coordinates = Attribute("Window coordinates limits dict(top_left: dict(y, x), bottom_right: dict(y,x))")

    def refresh():
        """Refresh the view, it has to be override"""

    def setup_layout():
        """Define the basic layer window"""

    def draw(border, title):
        """Draw the Form window"""


class ContainerBaseView(Interface):
    """Abstract container controller"""

    model = Attribute("Model reference of the Layout")
    position = Attribute("This is a vector(x:int and y:int) relative position "
                         "of this element inside a Form container")

    def draw_decorum():
        """Design content of container"""

    def draw_data_label():
        """Design the data content"""

    def draw_data_content():
        """Design the data content"""


class WidgetBaseView(Interface):
    """
    This is the abstract class of a Widget as a graphical element to  render inside a Form
    instructed by his Element container parent
    It has a margin, a padding,a content design, a data to contain linked to his model
    """

    margin: tuple = Attribute("A marge inside (top, right, bottom, left)")
    padding: tuple = Attribute("A pad space outside (top, right, bottom, left)")
    screen = Attribute("Widget has a screen to start design from")

    def draw():
        """Draw the widget"""

    def setup_screen(screen):
        """Setup the screen curses.initscr() to use"""
