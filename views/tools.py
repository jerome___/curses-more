"""Tools to use from any other views modules"""

import curses
from models.tools import Coordinates, Size



def define_edges() -> Coordinates:
    """Define new edges of screen and return tuple(y: int, x: int) """

    return Coordinates(y=curses.LINES - 1, x=curses.COLS - 1)



