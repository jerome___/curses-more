"""Errors module checker and Exception class to get from"""

from zope.interface import Invalid
from logging import getLogger

from models.forms import FormModel
from views.forms import FormView
from models.elements import WidgetModel, LayoutModel
from views.elements import WidgetView, LayoutView


class TypeErrorAttributes(Invalid):
    """Return if Form implemented class with invalid type has been used"""

    def __init__(self, args, **kwargs):

        self._args = args
        self.kwargs = kwargs
        logger = getLogger('Abstract Controller TypeErrorFormAttribute')
        what = ""
        i = 0
        if kwargs.get('model'):
            what = "model"
            i += 1
        if kwargs.get('view'):
            what += "view" if i == 0 else " and view"
        logger.error("wrong type for %s", what)


def check_attributes(att):
    """Test if FormC attribute is a FomModel for self.model"""

    kwargs = dict()
    if not isinstance(att.model, (FormModel, WidgetModel, LayoutModel)):
        kwargs['model'] = True
    if not isinstance(att.view, (FormView, WidgetView, LayoutView)):
        kwargs['view'] = True
    if kwargs:
        raise TypeErrorAttributes(att, **kwargs)
