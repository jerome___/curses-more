"""Elements module define:
 _ Element parent for Containers (Layout) and Widgets
 _ Layout container parent
 _ Widget object parent"""


from zope.interface import implementer, Interface, Attribute, Invalid, invariant
from abc import ABCMeta, ABC, abstractmethod

from models.elements import ListLayoutModel, TableLayoutModel, WidgetModel
from views.elements import VerticalLayoutView, HorizontalLayoutView, GridLayoutView, WidgetView
from .tools import *


class ElementBase(Interface):
    """Element has a Widget or Container child"""

    view = Attribute("Is the child view")
    model = Attribute('is the child model')
    name = Attribute('is a unique name of child')


class ContainerBase(Interface):
    """
    A container has a type of disposition in charge of the physical organisation of his content
    It contain one or any Layouts of one or any Widgets container
    and a vector default relative position (readable only)
    and a status
    """

    def add(content):
        """Append a content to the content_list (Container or Widget view)"""

    def insert(content, position):
        """Insert a content at position relative place in the content_list"""

    def get(position):
        """Return an object from content list (do not delete it)"""

    def remove(position):
        """Remove an object from positioned content list"""


class WidgetBase(Interface):
    """
    A Widget can contain one or any:
    - data content (can be edited maybe),
    - some special key binding actions
    - a Label content
    - a link between data to print in screen and data to red/write from Database or any other content reference
    A widget have to contain a position in relation with a Layout linked with
    """

    def refresh():
        """To refresh widget content and position and size"""

    def setup_key_binding():
        """Setup the binding special keys to use with Widget"""


@implementer(ElementBase)
class Element:
    """Element is parent of Widget or Containers (Layouts)
    it is not a concrete class and can not be implemented"""

    def __init__(self, **kwargs):

        self._name = kwargs.get('name')
        self._view = kwargs.get('view')
        self._model = kwargs.get('model')

    @property
    def model(self):
        return self._model

    @property
    def view(self):
        return self._view

    @property
    def name(self):
        return self._name


@implementer(ContainerBase)
class Layout(Element, ABC):
    """Contain disposition of content containers and widgets, and is a Mixin object
    (do not implement, it is not a concrete class)"""

    @abstractmethod
    def __init__(self, **kwargs):
        """abstract class"""
        super().__init__(**kwargs)

    def __add__(self, element):
        """Append a content to the content_list (Container or Widget view)"""

        self._model += element
        self._view.refresh()
        return self

    def insert(self, element, position):
        """Insert a content at position relative place in the content_list"""

        self._model.insert(element, position)
        self._view.refresh()

    def __getitem__(self, position):
        """Return an object from content list (do not delete it)
        position can be a int or a tuple"""

        return self._model[position]

    def __delitem__(self, position):
        """Remove an object from positioned content list"""

        del self._model[position]
        self._view.refresh()


@unique('name')
class VerticalLayout(Layout):
    """Layout with list of vertical Elements inside"""

    def __init__(self, **kwargs):

        super().__init__(model=ListLayoutModel(**kwargs),
                         view=VerticalLayoutView(**kwargs),
                         **kwargs)


@unique('name')
class HorizontalLayout(Layout):
    """Layout with list of horizontal Elements inside"""

    def __init__(self, **kwargs):

        super().__init__(model=ListLayoutModel(**kwargs),
                         view=HorizontalLayoutView(**kwargs),
                         **kwargs)


@unique('name')
class GridLayout(Layout):
    """Layout with list of list for grid/table Elements inside"""

    def __init__(self, **kwargs):

        super().__init__(model=TableLayoutModel(**kwargs),
                         view=GridLayoutView(**kwargs),
                         **kwargs)


@implementer(WidgetBase)
class Widget(Element):
    """Widget has required minimal attributes and methods to be use by other typed Widgets"""

    def __init__(self, **kwargs):

        super().__init__(**kwargs)
        self._model = WidgetModel(**kwargs)
        self._view = WidgetView(**kwargs)

    def define_label(self, label):
        """Define model and view label to print"""

        self._model.label = label
        self._view.label_width = len(label) + 2

    def setup_key_binding(self):
        """Special minimal Widget key binding setup"""

        pass

    def refresh(self):
        """Minimal refresh Widget actions"""

    @property
    def name(self):
        """Get name of Widget"""

        return self._name
