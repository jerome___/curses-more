"""Forms Controllers Module"""

from zope.interface import implementer, Interface, Attribute, Invalid, invariant
from enum import Enum

from models.forms import FormModel
from views.forms import FormView
from .tools import *
from controllers.errors import check_attributes


class FormBase(Interface):
    """Form Parent to inherit for all the Forms objects of curses-more framework use"""

    view: FormView = Attribute("Ui Form view (FormView)")
    model: FormModel = Attribute("Model Form (FormModel)")

    invariant(check_attributes)

    def update():
        """Update all Form's contents statement (as to be override)"""

    def setup_key_binding():
        """Key's binding model handler to react with. It has to be override"""

    def setup_widgets():
        """Setup widgets" content"""

    def move_selection(direction):
        """Move to next/previous Widget|Container depend on direction and Layout"""

    def add_element(element, position):
        """Add an element (Widget or Container) with optionally his position"""

    def run():
        """Main run loop for the form controller
        where link between keys/mouse events are linked with actions"""


class Direction(Enum):
    """direction to move from widget to another"""

    Up = 0
    Down = 0
    Right = 2
    Left = 2



@unique('name')
@implementer(FormBase)
class Form:
    """Form with a menu
    methods start with action_ have to return bool: False for quit application
    they also have to be referenced inside self._key_binding dict values"""

    __NUMBER = 0

    def __init__(self, **kwargs):

        cls = self.__class__
        cls.__NUMBER += 1
        self._name = kwargs.get('name')
        self._key_binding = dict()
        self._model = FormModel(cls.__NUMBER, **kwargs)
        self._view = FormView(**kwargs)
        if not kwargs.get('test'):
            self._model.theme = kwargs
            self._view.draw(self._model.border, self._model.title)
            wl = self._view.window_limit
            self._model.cursor = wl.TopLeft
            self.setup_key_binding()

    def __del__(self):

        cls = self.__class__
        if cls.__NUMBER != 0:
            cls.__NUMBER -= 1

    def setup_key_binding(self):
        """Setup keys binding to handle for FormMenu"""

        self.key_binding = {
            "q": lambda: False,                     # stop the application
            "p": self.action_print_something,
            "curses.KEY_UP": self.move_selection(Direction.Up),
            "curses.KEY_DOWN": self.move_selection(Direction.Down),
            "curses.KEY_RIGHT": self.move_selection(Direction.Right),
            "curses.KEY_LEFT": self.move_selection(Direction.Left),
        }

    def setup_widgets(self):
        """Setup widgets content"""

        for widget in self._widgets:
            widget.refresh()

    def run(self):
        """Main loop linker between keys/mouse event and actions concerned"""

        state = True
        while state:
            self._view.refresh()
            key = self._view.get_key()
            if key in self._key_binding:
                state = self.key_binding[key]()

    def move_selection(self, direction: Direction):
        """Move to next/previous Widget|Container element depend on direction and own Layout organization"""

        pass

    def add_element(self, element, position=None):
        """Add an element to self._model._elements"""

        if position:
            self._model.elements[position] = element
        else:
            self._model.elements.append(element)

    def action_print_something(self):
        """Print something to try"""

        self._view.window.addstr(0, 0, "TEST")
        return True

    def update(self):
        """Update this content's objects statement"""
        pass

    @property
    def model(self):
        return self._model

    @property
    def view(self):
        return self._view

    @property
    def key_binding(self):
        """Get key binding dict events: key is curses handler, value concerned is action to call from controller"""

        return self._key_binding

    @key_binding.setter
    def key_binding(self, key_binding: dict):
        """Update key_binding"""

        self._key_binding.update(key_binding)
