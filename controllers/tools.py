"""Tools module for controllers"""


from functools import wraps


class KwargsMissing(Exception):
    """Instance object doesn't have required  kwargs key present"""

    def __repr__(self):
        return self.__doc__


class KwargsValueExistAlready(Exception):
    """Instance object has a required kwargs value who exist already (should be unique)"""

    def __repr__(self):
        return self.__doc__


def unique(*args):
    """Decorator 'unique' define unique kwargs keys as args
    _ to be present at object creation
    _ value of each to be unique"""

    existing_kwargs = dict((k, []) for k in args)

    def decorator(function):
        wraps(function)

        def wrapper(*wargs, **kwargs):
            if all(k in kwargs.keys() for k in existing_kwargs.keys()):
                new_kwargs = dict()
                for k in existing_kwargs.keys():
                    if kwargs[k] in existing_kwargs[k]:
                        raise KwargsValueExistAlready("'{}' should have unique value '{}'".format(k, kwargs[k]))
                    new_kwargs[k] = kwargs[k]
                for k in new_kwargs.keys():
                    existing_kwargs[k].append(new_kwargs[k])
            else:
                raise KwargsMissing("'{}' should be present in kwargs keys".format(*args))
            return function(*wargs, **kwargs)
        return wrapper
    return decorator
