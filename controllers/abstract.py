"""Abstract Controllers classes for Forms, Widgets or any other control framework object to inherit"""

from zope.interface import Interface, Attribute, Invalid, invariant

from models.forms import FormModel
from views.forms import FormView
from controllers.errors import check_attributes


class WindowBase(Interface):
    """Window is the first curses screen to inherit curses.initscr()
    And it has  one or many Forms"""

    screen = Attribute("curses.initscr()")
    forms: list = Attribute("List of Form's children from one to many")

    def add_form(form):
        """Add a form instance to forms list"""


